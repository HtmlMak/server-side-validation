var $ = require('gulp-load-plugins')();
var argv = require('yargs').argv;
var gulp = require('gulp');

var ISPRODUCTION = !!(argv.production);
var ISDEVELOPMENT = !!(argv.development);
var SUFFIXMIN = ISPRODUCTION ? '.min' : '';

var PATHS = {
    dist: 'dist/',
    src: ['src/server-side-validation.js', 'vendor/jquerytools/validator.js']
};

gulp.task('full-pack', function () {
    var uglify = $.if(ISPRODUCTION, $.uglify()
        .on('error', function (e) {
            console.log(e);
        }));

    return gulp.src(PATHS.src)
        .pipe($.sourcemaps.init())
        .pipe($.concat('server-side-validation.full'+ SUFFIXMIN +'.js'))
        .pipe(uglify)
        .pipe($.if(ISDEVELOPMENT, $.sourcemaps.write()))
        .pipe(gulp.dest(PATHS.dist));
});

gulp.task('js', function () {
    var uglify = $.if(ISPRODUCTION, $.uglify()
        .on('error', function (e) {
            console.log(e);
        }));

    return gulp.src(PATHS.src)
        .pipe(uglify)
        .pipe(gulp.dest(PATHS.dist));
});

gulp.task('watch', ['js', 'full-pack'], function () {
    gulp.watch(PATHS.src, ['js', 'full-pack']);
});