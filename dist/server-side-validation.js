/**
 * Created by PhpStorm.
 * User: htmlmak
 * Date: 05.11.2016
 * Time: 23:11
 */

(function (window) {
    window.Validator = function (param) {
        this.settings = $.extend(true, {
            showDuration: 5000,
            showSpeed: 150,
            text: {
                success: 'Ваше сообщение отправлено',
                error: '<h5>Произошла ошибка.</h5><p>Повторите попытку позднее. В случае повторения ошибки свяжитесь с администрацией.</p>'
            },
            classes: {
                form: 'server-side-form',
                elements: 'server-side-form__elements',
                success: 'server-side-form__success',
                error: 'server-side-form__errors',
                field: {
                    wrapper: 'field',
                    input: 'input, textarea',
                    label: 'label',
                    error: 'server-side-form__error'
                }
            },
            callback: {
                done: null,
                fail: null,
                always: null
            }
        }, param);

        /**
         * Функция для привязки серверной валидации к форме
         * @param selector - селектор
         * @param {Object=}options - параметры
         */
        this.bind = function (selector, options) {
            var _ = this;
            var $form = null;

            return $(selector).each(function (i, form) {
                $form = $(form);

                options = $.extend(_.settings, options, $form.data());

                _.setCallback(options.callback);
                _.checkStructureForm.bind(_)($form);

                $form.validator({effect: 'labelMate'}).submit(function (e) {
                    e.preventDefault();

                    $.ajax({
                        url: $form.attr('action'),
                        data: $form.serialize(),
                        dataType: 'JSON',
                        method: 'POST',
                        beforeSend: function () {
                            _.toggleLockForm($form);
                            _.hideErrorMessage(options, $form);
                        }
                    }).success(function (data) {
                        options.callback.done.bind(_)($form, data, options);
                    }).error(function (data) {
                        options.callback.fail.bind(_)($form, data, options);
                    }).complete(function (data) {
                        options.callback.always.bind(_)($form, data, options);
                    });
                });
            });
        };

        this.bindAddEffect();
    };

    Validator.prototype.convertErrors = function (errors) {
        var result = {};

        $.each(errors, function (index, error_list) {
            result[index] = error_list.join('<br>');
        });

        return result;

    };

    Validator.prototype.bindAddEffect = function () {
        /**
         * Запутанная схема для добавления и удаления классов у элементов,
         * в зависимости от наличия ошибок. Править вдумчиво и осторожно.
         */

        var _ = this;

        $.tools.validator.addEffect("labelMate", function (errors) {
            $.each(errors, function (index, error) {
                var $fieldWrap = error.input.first().closest('.' + _.settings.classes.field.wrapper);

                $fieldWrap.find(_.settings.classes.field.input).addClass('is-invalid-input');
                $fieldWrap.find(_.settings.classes.field.label).addClass('is-invalid-label');

                if (!$fieldWrap.find('.' + _.settings.classes.field.error).length) {
                    $fieldWrap.append($('<div>', {
                        class: _.settings.classes.field.error
                    }));
                }

                $fieldWrap.find('.' + _.settings.classes.field.error).text(error.messages[0]).addClass('is-visible');
            });
        }, function (inputs) {
            inputs.each(function () {
                var $fieldWrap = $(this).closest('.' + _.settings.classes.field.wrapper);

                $fieldWrap.find(_.settings.classes.field.input).removeClass('is-invalid-input');
                $fieldWrap.find(_.settings.classes.field.label).removeClass('is-invalid-label');
                $fieldWrap.find('.' + _.settings.classes.field.error).removeClass('is-visible');
            });
        });
    };

    Validator.prototype.getButtonOnForm = function ($form) {
        var $result = $form.find('button[type="submit"]');

        if (!$result.length) {
            $result = $('body').find('button[form="' + $form.attr('id') + '"]');
        }

        return $result;
    };

    Validator.prototype.toggleLockForm = function ($form) {
        if ($form.hasClass('lock')) {
            $form.removeClass('lock');
            this.getButtonOnForm($form).removeClass('disabled');
        } else {
            $form.addClass('lock');
            this.getButtonOnForm($form).addClass('disabled');
        }
    };

    Validator.prototype.hideErrorMessage = function (settings, $form) {
        $form.find('.' + settings.classes.error).fadeOut(settings.showSpeed);
    };

    Validator.prototype.showErrorMessage = function (settings, $form) {
        $form.find('.' + settings.classes.error).text(settings.text.error).fadeIn(settings.showSpeed);
    };

    Validator.prototype.checkStructureForm = function ($form) {
        /* Добавление своих классов к форме */
        var _ = this;
        $form.addClass(_.settings.classes.form);

        if (!$form.find('.' + _.settings.classes.success).length) {
            $form.append($('<div>', {
                class: _.settings.classes.success,
                text: _.settings.text.success,
                style: 'display: none;'
            }));
        }
        if (!$form.find('.' + _.settings.classes.error).length) {
            $form.prepend($('<div>', {
                class: _.settings.classes.error,
                text: _.settings.text.error,
                style: 'display: none;'
            }));
        }
        if (!$form.find('.' + _.settings.classes.elements).length) {
            $form.children('*')
                .not('.' + _.settings.classes.success)
                .not('.' + _.settings.classes.error)
                .wrapAll(
                    $('<div>', {
                        class: _.settings.classes.elements
                    })
                );
        }

    };

    Validator.prototype.setCallback = function (callback) {
        if (callback.done == null) {

            callback.done = function ($form, data, options) {
                if (data.error_code == 0) {
                    $form.css('height', $form.height());
                    $form.find('.' + options.classes.elements).fadeOut(options.showSpeed);
                    $form.find('.' + options.classes.success)
                        .html(data.result[0] || options.text.success)
                        .fadeIn(options.showSpeed);

                    setTimeout(function () {
                        $form.get(0).reset();
                        $form.find('.' + options.classes.elements).fadeIn(options.showSpeed);
                        $form.find('.' + options.classes.success).fadeOut(options.showSpeed);
                    }, options.showDuration);
                }
                else {
                    $form.data('validator').invalidate(this.convertErrors(data.error_messages));
                }
            }
        }

        if (callback.fail == null) {
            callback.fail = function ($form, data, options) {
                this.showErrorMessage(options, data);
            }
        }

        if (callback.always == null) {
            callback.always = function ($form, data, options) {
                this.toggleLockForm($form);
            }
        }
    };
})(window);

